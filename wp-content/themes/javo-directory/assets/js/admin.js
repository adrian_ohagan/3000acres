;( function( $ ){

	var javo_directory_admin_js = function(){

		this.init();

	}

	javo_directory_admin_js.prototype = {

		constructor : javo_directory_admin_js,

		init : function(){

			this

				// Theme Settings
				.theme_settings()

				// Hide Content Settings
				.hidePMProContentSettings();
		},

		theme_settings : function() {

			var obj = this;

			// Map > Default Position
			obj.map_default_location();

			return obj;
		},

		hidePMProContentSettings : function() {

			var
				obj = this,
				wrap = $( ".wrap.pmpro_admin" )
				options = $( 'input[name^="membershipcategory_"]' )
				options_wrap = options.closest( '.form-table' )
				options_header = options_wrap.prev( 'h3' );

			options_wrap.remove();
			options_header.remove();

			return obj;

		},

		map_default_location : function() {

			var
				obj = this,
				element = $( '.javo-ts-map-default-setting-container' ),
				inputLat	= $( '[name="javo_ts[map][default_lat]"]' ),
				inputLng = $( '[name="javo_ts[map][default_lng]"]' ),
				floatLat	= parseFloat( inputLat.val() || 0 ),
				floatLng = parseFloat( inputLng.val() || 0 ),
				objLatLng = new google.maps.LatLng( floatLat, floatLng );

			$( document ).on( 'click', '[href="#javo-ts-map-default-setting-view"]',
				function( e ){

					e.preventDefault();

					element
						.height( 300 )
						.gmap3({

							map:{
								options:{ center: objLatLng },
								events:{
									click: function( map, curPosition ){

										$( this ).gmap3({
											get:{
												name: 'marker',
												callback: function( marker ){

													marker.setPosition( curPosition.latLng );
													inputLat.val( marker.getPosition().lat() );
													inputLng.val( marker.getPosition().lng() );

												}
											}
										});

									}
								}
							},
							marker : {
								latLng : objLatLng,
								options : { draggable : true },
								events : {
									dragend:function( m ){
										inputLat.val( m.getPosition().lat() );
										inputLng.val( m.getPosition().lng() );
									}
								}
							}
						});
					$( this ).remove();
				}
			);
		},
	}

	new javo_directory_admin_js;

} )( jQuery );