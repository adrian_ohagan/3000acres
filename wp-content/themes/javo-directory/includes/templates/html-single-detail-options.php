<div class="col-md-12 col-xs-12 item-condition" id="javo-item-condition-section" data-jv-detail-nav>
	<h3 class="page-header"><?php esc_html_e( "Item detail", 'javospot' ); ?></h3>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row summary_items">
				<div class="col-sm-12 col-xs-12 jv-summary-detail-wrap">
					<?php if(''!=(get_post_meta( get_the_ID(), '_website', true ))){?>
					<div class="row jv-meta-website">
						<div class="col-md-4 col-xs-12">
							<span><?php esc_html_e( "Website", 'javospot' );?></span>
						</div>
						<div class="col-md-8 col-xs-12">
							<span><a href="<?php echo esc_url(get_post_meta( get_the_ID(), '_website', true ));?>" target="_blank"><?php echo esc_html(get_post_meta( get_the_ID(), '_website', true ));?></a></span>
						</div>
					</div><!-- /.row *website -->
					<?php }
					if(''!=(get_post_meta( get_the_ID(), '_email', true ))){?>
					<div class="row jv-meta-email">
						<div class="col-md-4 col-xs-12">
							<span><?php esc_html_e( "Email", 'javospot' );?></span>
						</div>
						<div class="col-md-8 col-xs-12">
							<span><?php echo esc_html(get_post_meta( get_the_ID(), '_email', true ));?></span>
						</div>
					</div><!-- /.row *email -->
					<?php }
					if(''!=(get_post_meta( get_the_ID(), '_address', true ))){?>
					<div class="row jv-meta-address">
						<div class="col-md-4 col-xs-12">
							<span><?php esc_html_e( "Address", 'javospot' );?></span>
						</div>
						<div class="col-md-8 col-xs-12">
							<span><?php echo esc_html(get_post_meta( get_the_ID(), '_address', true ));?></span>
						</div>
					</div><!-- /.row *address -->
					<?php } ?>
					<?php if(''!=(get_post_meta( get_the_ID(), '_phone1', true ))){?>
					<div class="row jv-meta-phone1">
						<div class="col-md-4 col-xs-12">
							<span><?php esc_html_e( "Phone 1", 'javospot' );?></span>
						</div>
						<div class="col-md-8 col-xs-12">
							<span><a href="tel://<?php echo esc_html(get_post_meta( get_the_ID(), '_phone1', true ));?>"><?php echo esc_html(get_post_meta( get_the_ID(), '_phone1', true ));?></a></span>
						</div>
					</div><!-- /.row *phone1-->
					<?php }
					if(''!=(get_post_meta( get_the_ID(), '_phone2', true ))){?>
					<div class="row jv-meta-phone2">
						<div class="col-md-4 col-xs-12">
							<span><?php esc_html_e( "Phone 2", 'javospot' );?></span>
						</div>
						<div class="col-md-8 col-xs-12">
							<span><a href="tel://<?php echo esc_html(get_post_meta( get_the_ID(), '_phone2', true ));?>"><?php echo esc_html(get_post_meta( get_the_ID(), '_phone2', true ));?></a></span>
						</div>
					</div><!-- /.row *phone2-->
					<?php }
					$listing_keyword = '';
					if($listing_keyword = esc_html(lava_directory_terms( get_the_ID(), 'listing_keyword' ))){?>
					<div class="row jv-meta-keyword">
						<div class="col-md-4 col-xs-12">
							<span><?php esc_html_e( "Keyword", 'javospot' );?></span>
						</div>
						<div class="col-md-8 col-xs-12">
							<span><i><?php echo $listing_keyword; ?></i></span>
						</div>
					</div><!-- /.row *phone2-->
					<?php } ?>
				</div><!--/.col-md-12.jv-summary-detail-wrap -->
			</div><!--/.summary_items -->
		</div><!--/.panel-body -->
	</div><!--/.panel panel-default -->
</div><!--/.col-md-12 -->