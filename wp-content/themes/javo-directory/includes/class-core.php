<?php
if( !defined( 'ABSPATH' ) )
	die;

class jvfrm_spot_Directory
{
	/**
	 *	Required Initialize Settings
	 */
	const SLUG			= 'lv_listing';
	const SUPPORT		= 'lv_support';
	const NAME			= 'listing';
	const CORE			= 'Javo_Spot_Core';
	const FEATURED_CAT	= '_category';
	const MAINPLUG		= 'Lava_Directory_Manager';

	/**
	 *	Additional Initialize Settings
	 */
	const REVIEW				= 'Lava_Directory_Review';
	private static $instance;

	public $slug;
	protected $template_path	= false;


	public static function get_instance( $file ) {
		if( !self::$instance )
			self::$instance = new self( $file );

		return self::$instance;
	}

	public function __construct( $file ) {
		$this->initialize( $file );
		$this->load_files();
		$this->register_hoook();
		$this->shortcode = new jvfrm_spot_Directory_Shortcode;
	}

	public function initialize( $file ) {
		$this->file				= $file;
		$this->folder			= ( dirname( $this->file ) );
		$this->dir				= trailingslashit( JVFRM_SPOT_THEME_DIR . '/includes' );
		$this->assets_dir		= trailingslashit( $this->dir . 'assets' );
		$this->path				= dirname( $this->file );
		$this->template_path	= trailingslashit( $this->path ) . 'templates';

		$this->slug				= self::SLUG;
	}

	public function load_files()
	{
		require_once "class-template.php";
		require_once "class-shortcode.php";
		require_once "function-lv_listing.php";
		require_once "function-lv_support.php";
		require_once "vc-core.php";
		require_once "the-grid-core.php";
	}

	public function register_hoook()
	{
		add_action( 'init', array( $this, 'load_templateClass' ) );
		// Require Plugins
		add_action( 'jvfrm_spot_tgmpa_plugins', Array( $this, 'spot_tgmpa_plugins' ) );
		add_action( 'jvfrm_spot_helper_require_plugins', Array( $this, 'helper_require_plugins' ) );
		add_action( 'jvfrm_spot_helper_require_plugins_pass', Array( $this, 'helper_require_plugins_bool' ) );

		add_action( 'wp_enqueue_scripts', Array( $this, 'register_resources' ) );
		add_action( 'init', Array( $this, 'custom_object' ), 100 );
		add_filter( 'lava_' . self::SLUG . '_json_addition', Array( $this, 'json_append' ), 10, 3 );

		add_filter( 'jvfrm_spot_theme_setting_pages', Array( $this, 'main_slug_page' ) );
		add_filter( 'jvfrm_spot_theme_setting_pages', Array( $this, 'map_page' ) );

		// Enqueue Css
		add_filter( 'jvfrm_spot_enqueue_css_array', Array( $this, 'enqueue_php_css_array' ) );

		// Enqueue Less
		add_filter( 'jvfrm_spot_enqueue_less_array', Array( $this, 'enqueue_php_less_array' ) );

		// Dashbaord
		add_filter( 'lava_' . self::SLUG . '_new_item_redirect' , Array( $this, 'new_item_redirect' ), 15, 2 );
		add_filter( 'jvfrm_spot_dashboard_slugs' , Array( $this, 'custom_register_slug' ) );
	}

	public function load_templateClass() {
		$this->template = new jvfrm_spot_Directory_Template;
	}

	public function getSlug() {
		return self::SLUG;
	}

	public function getCoreName( $suffix=false ){
		$strSuffix = $suffix ? '_' . $suffix : false;
		return self::CORE . $strSuffix;
	}

	public function getHandleName( $strName='' ){ return sanitize_title( 'jv-' . $strName ); }

	public function spot_tgmpa_plugins( $plugins=Array() ) {
		return wp_parse_args(
			Array(
				// Contact Form
				array(
					'name'						=> 'Contact Form 7',
					'slug'						=> 'contact-form-7',
					'required'					=> false,
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-contact-form-7.png',
				),

				// Lava Directory Manager
				Array(
					'name'						=> 'Lava Directory Manager',
					'slug'						=> 'lava-directory-manager',
					'required'					=> true,
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-lava-directory-manager.png',
				),

				// Javo Spot Core
				Array(
					'name' => 'Javo Directory Core',
					'slug' => 'javo-spot-core',
					'version' => '1.0.12',
					'required' => true,
					'force_activation' => false,
					'force_deactivation' => false,
					'external_url' => '',
					'source' => get_template_directory() . '/library/plugins/javo-spot-core.zip',
					'image_url' => JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-javo-map-framework-core-logo.png',
				),

				// Slider Revolution
				Array(
					'name'						=> 'Revolution Slider',
					'slug'						=> 'revslider',
					'version'					=> '5.4.1',
					'required'					=> true,
					'force_activation'			=> false,
					'force_deactivation'		=> false,
					'external_url'				=> '',
					'source'					=> get_template_directory() . '/library/plugins/revslider.zip',
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-revslider.png',
				),

				// NinjaForms
				Array(
					'name'						=> 'Ninja Forms',
					'slug'						=> 'ninja-forms',
					'required'					=> false,
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-ninja-forms.png',
				),

				// Visual Composer
				array(
					'name'						=> 'WPBakery Visual Composer', // The plugin name
					'slug'						=> 'js_composer', // The plugin slug (typically the folder name)
					'source'					=> get_template_directory() . '/library/plugins/js_composer.zip', // The plugin source
					'required'					=> true, // If false, the plugin is only 'recommended' instead of required
					'version'					=> '5.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation'			=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation'		=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url'				=> '', // If set, overrides default API URL and points to an external URL
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-js_composer.png',
				),

				// Ultimate Addons
				array(
					'name'						=> 'Ultimate Addons for Visual Composer', // The plugin name
					'slug'						=> 'Ultimate_VC_Addons', // The plugin slug (typically the folder name)
					'source'					=> get_template_directory() . '/library/plugins/Ultimate_VC_Addons.zip', // The plugin source
					'required'					=> true, // If false, the plugin is only 'recommended' instead of required
					'version'					=> '3.16.8', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation'			=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation'		=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url'				=> '', // If set, overrides default API URL and points to an external URL
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-Ultimate_VC_Addons.png',
				),
				// The Grid
				array(
					'name'						=> 'The Grid', // The plugin name
					'slug'						=> 'the-grid', // The plugin slug (typically the folder name)
					'source'					=> get_template_directory() . '/library/plugins/the-grid.zip', // The plugin source
					'required'					=> true, // If false, the plugin is only 'recommended' instead of required
					'version'					=> '2.3.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation'			=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation'		=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url'				=> '', // If set, overrides default API URL and points to an external URL
					'image_url'					=> JVFRM_SPOT_IMG_DIR . '/icon/jv-default-setting-plugin-javo-the-grid-core-logo.png',
				),
			), $plugins
		);
	}

	public function register_resources() {
		$jvfrm_spot_load_styles =
		Array(
			'single.css'									=> '0.1.0',
		);

		$jvfrm_spot_load_scripts =
		Array(
			'single.js' => '1.0.0',
			'map-template.js' => '1.0.0',
			'jquery.javo_search_shortcode.js'	=> '0.1.0',
		);


		if( !empty( $jvfrm_spot_load_styles ) ) : foreach( $jvfrm_spot_load_styles as $filename => $version ) {
			wp_register_style(
				$this->getHandleName( $filename ),
				$this->assets_dir . "css/{$filename}",
				Array(),
				$version
			);
		} endif;

		if( !empty( $jvfrm_spot_load_scripts ) ) : foreach( $jvfrm_spot_load_scripts as $filename => $version ) {
			wp_register_script(
				$this->getHandleName( $filename ),
				$this->assets_dir . "js/{$filename}",
				Array( 'jquery' ),
				$version,
				true
			);
		} endif;
	}

	public function json_append( $args, $post_id, $objTerm ) {
		global $lava_directory_manager_func;
		$arrResult			= Array();
		$arrAllMeta		= Array_Keys( apply_filters( 'lava_' . self::SLUG . '_more_meta', Array() ) );
		$arrExcludes		= Array( '_phone1', '_phone2', '_address', '_email', '_website' );

		$arrAllMeta		= array_diff( $arrAllMeta, $arrExcludes );

		if( !empty( $arrAllMeta ) )  foreach( $arrAllMeta as $metaKey )
			$arrResult[ $metaKey ] = get_post_meta( $post_id, $metaKey, true );

		$arrResult[ 'f' ] = get_post_meta( $post_id, '_featured_item', true );

		return wp_parse_args( $arrResult, $args );
	}

	public function main_slug_page( $pages ){
		return wp_parse_args(
			Array(
				'lv_listing'			=> Array(
					esc_html__( "Listing", 'javospot' ), false
					, 'priority'		=> 35
					, 'external'		=> $this->template_path . '/admin-theme-settings-item.php'
				)
			)
			, $pages
		);
	}

	public function map_page( $pages ) {
		return wp_parse_args(
			Array(
				'map'			=> Array(
					esc_html__( "Map", 'javospot' ), false
					, 'priority'		=> 32
					, 'external'		=> $this->template_path . '/admin-theme-settings-map.php'
				)
			)
			, $pages
		);
	}

	public function enqueue_php_css_array( $csses=Array() ){
		return wp_parse_args(
			Array(
				'includes-assets-extra' => Array(
					'dir' => $this->assets_dir . 'css',
					'file' => 'extra.css',
				)
			),
			$csses
		);
	}

	public function enqueue_php_less_array( $lesses=Array() ) {
		return wp_parse_args(
			Array(
				'includes-assets-extra' => Array(
					'dir' => $this->assets_dir . 'css',
					'file' => 'extra.less',
				)
			),
			$lesses
		);
	}

	public function helper_require_plugins( $plugins=Array() ) {
		return wp_parse_args(
			Array(
				'Javo_Spot_Core'				=> esc_html__( "Javo Directory Core", 'javospot' ),
				'Lava_Directory_Manager'	=> esc_html__( "Lava Directory Manager", 'javospot' )
			), $plugins
		);
	}

	public function helper_require_plugins_bool( $boolPass=false ) {
		return $boolPass && class_exists( 'Javo_Spot_Core' ) && function_exists( 'lava_directory' );
	}

	public function new_item_redirect( $URL, $post_id ){
		
		$is_update = isset( $_POST[ 'post_id' ] ) && intVal( $_POST[ 'post_id' ] ) > 0;

		if( $is_update )
			return $URL;

		if( function_exists( 'lv_directory_payment' ) ) {
			$strSLUG = 'add-' . self::SLUG;
			if( lv_directory_payment()->core->is_using )
				$URL = esc_url( add_query_arg( Array( 'pay' => $post_id ), jvfrm_spot_getCurrentUserPage( $strSLUG ) ) );
		}
		return $URL;
	}

	public function custom_register_slug( $args ) {
		return wp_parse_args(
			Array('JVFRM_SPOT_ADDITEM_SLUG'			=> 'add-'.self::SLUG )
			, $args
		);
	}

	public function custom_object() {
		// Exclude Search
		$objPostType = get_post_type_object( self::SLUG );

		if( is_object( $objPostType ) )
			$objPostType->exclude_from_search = true;
	}
}

if( !function_exists( 'jvfrm_spot_core' ) ) {
	function jvfrm_spot_core() {
		$objInstance				= jvfrm_spot_Directory::get_instance( __FILE__ );
		$GLOBALS[ 'jvfrm_spot_Directory' ]	= $objInstance;
		return $objInstance;
	}
	jvfrm_spot_core();
}