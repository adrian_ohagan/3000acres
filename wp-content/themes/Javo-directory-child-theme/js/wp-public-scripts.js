jQuery( document ).ready(function($) {	
	
	$( document ).on( 'click', '.wp_popup_share a', function(e) {
		e.preventDefault();		
		window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
	});
	
	$( '.wp_mail a').attr( 'target', '_blank' );
	$( document ).on( 'click', '.noUi-handle', function(e) {
		alert("Please click on auto locate icon to the left in order to change the distance");
	});
	$( '.javo-map-tab-panel' ).removeClass( 'hidden' );
});