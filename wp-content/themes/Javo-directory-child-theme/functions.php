<?php
// Translate function for child theme 
// if you need to translate strings in child theme, please remove these comment and use this function

// load_child_theme_textdomain( 'javo_fr', get_stylesheet_directory() . '/languages' );
//add_filter('javo_dashboard_custom_template_url','javo_child_dashboard_template_url',10,2);
 
/*function  javo_child_dashboard_template_url($url,$subpage){
	echo "<pre>";
	print_r($subpage);
	//echo "<pre>";exit();
	/*if($subpage=='followed'){
		$url=get_stylesheet_directory_uri().'/library/dashboard/'.$subpage.'.php';
		return $url;
	}	*/
	
//}//

/*
 * Added script at frontend side
 * Added by @wpweb
*/
add_action( 'wp_enqueue_scripts', 'wp_public_scripts' );
function wp_public_scripts() {	
	wp_register_script( 'wp-public-script',  get_stylesheet_directory_uri(). '/js/wp-public-scripts.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'wp-public-script' );
}

add_filter( 'javo_featured_menus_filter' ,'wp_javo_featured_menus_filter' ,10 ,1);

function wp_javo_featured_menus_filter( $javo_featured_menus ){
	
	global $javo_tso_db;
	
	$javo_featured_menus['write' ]	='';
	$javo_featured_menus['x_write'] ='';
	$javo_featured_menus['list']	='';
	
	$javo_nav_sys_buttons = Array();

	$javo_nav_sys_buttons['edit_profile']	= Array(
		'url'		=> home_url( JAVO_DEF_LANG . JAVO_MEMBER_SLUG . '/' . wp_get_current_user()->user_login . '/' . JAVO_PROFILE_SLUG )
		, 'label'	=> __("Edit Profile", 'javo_fr')
	);

	/* Items */
	if( $javo_tso_db->get( JAVO_ADDITEM_SLUG, '' ) != "disabled" )
	{
		$javo_nav_sys_buttons['my_sites']	= Array(
			'url'		=> home_url( JAVO_DEF_LANG . JAVO_MEMBER_SLUG . '/' . wp_get_current_user()->user_login . '/' . JAVO_ITEMS_SLUG )
			, 'label'	=> __("My Sites", 'javo_fr')
		);
	}
	if( $javo_tso_db->get( JAVO_ADDITEM_SLUG, '' ) != "disabled" )
	{
		$javo_nav_sys_buttons['add_sites']	= Array(
			'url'		=> home_url( JAVO_DEF_LANG.JAVO_MEMBER_SLUG . '/' . wp_get_current_user()->user_login . '/' . JAVO_ADDITEM_SLUG )
			, 'label'	=> sprintf( __("Add %s", 'javo_fr' ), __("Sites", 'javo_fr') )
		);
	}

	/* Logged Out */
	$javo_nav_sys_buttons['logged_out']	= Array(
		'url'		=> wp_logout_url( home_url() )
		, 'label'	=> __("Logout", 'javo_fr')
	);
	/* Write Button */{
		ob_start();
		if( ! empty( $javo_nav_sys_buttons ) ) :
			?>
			<ul class="dropdown-menu" role="menu">
				<?php
				foreach( $javo_nav_sys_buttons as $button ) {
					echo "<li><a href=\"{$button['url']}\">{$button['label']}</a></li>";
				} ?>
			</ul>
			<?php
		endif;
		$javo_featured_write_append = ob_get_clean();
	}

	$javo_featured_menus['profile']['li_class']		= 'javo-navi-mypage-button dropdown right-menus javo-navi-mylist-button';
	$javo_featured_menus['profile']['a_class']		= 'topbar-ser-image dropdown-toggle nav-right-button button-icon-fix';
	$javo_featured_menus['profile']['a_attribute']	= 'data-toggle="dropdown" data-javo-hover-menu';
	$javo_featured_menus['profile']['append']		= $javo_featured_write_append;
	
	
	return $javo_featured_menus;
}
add_action( 'javo_header_featured_menu_before' , 'javo_header_featured_menu_before_add_username');
function javo_header_featured_menu_before_add_username(){
	global $current_user;
	if($current_user){
		echo '<li class="javo-navi-user-name"><span>'.ucfirst($current_user->user_login).'</span></li>';
	}
}
 