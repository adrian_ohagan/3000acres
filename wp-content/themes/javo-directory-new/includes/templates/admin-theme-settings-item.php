<div class="jvfrm_spot_ts_tab javo-opts-group-tab hidden" tar="lv_listing">
	<h2> <?php esc_html_e( "Listing Settings", 'javospot'); ?> </h2>

	<table class="form-table">
	<tr><th>
		<?php esc_html_e( "Single Listing", 'javospot');?>
		<span class="description">	</span>
	</th><td>

		<h4><?php esc_html_e( "Header Cover Style", 'javospot' ); ?></h4>
		<fieldset class="inner">
			<select name="jvfrm_spot_ts[lv_listing_single_header_cover]">
				<?php
				$arrSingleItemHeaderCover	= apply_filters(
					'jvfrm_spot_lv_listing_header',
					Array(
						''				=> esc_html__( "None", 'javospot' ),
						'shadow'	=> esc_html__( "Top & Bottom Shadow", 'javospot' ),
						'overlay'	=> esc_html__( "Full Cover Overlay", 'javospot' ),
					)
				);
				if( !empty( $arrSingleItemHeaderCover ) )
					foreach( $arrSingleItemHeaderCover as $classes => $label ) {
						printf(
							"<option value=\"{$classes}\" %s>{$label}</option>",
							selected( $jvfrm_spot_tso->get( 'lv_listing_single_header_cover', 'overlay' ) == $classes, true, false )
						);
					}
				?>
			</select>
		</fieldset>

		<h4><?php esc_html_e( "Widget Sticky", 'javospot' ); ?></h4>
		<fieldset class="inner">
			<select name="jvfrm_spot_ts[<?php echo jvfrm_spot_core()->slug;?>_single_sticky_widget]">
			<?php
			foreach(
				Array(
					''			=> esc_html__( "Enable", 'javospot' ),
					'disable'	=> esc_html__( "Disable", 'javospot' ),
				)
			as $strOption => $strLabel )
				printf(
					"<option value=\"{$strOption}\" %s>{$strLabel}</option>",
					selected( $jvfrm_spot_tso->get( jvfrm_spot_core()->slug . '_single_sticky_widget', '' ) == $strOption, true, false )
				);
			?>
			</select>
		</fieldset>

		<h4><?php esc_html_e( "Map max width", 'javospot' ); ?></h4>
		<fieldset class="inner">
			<select name="jvfrm_spot_ts[<?php echo jvfrm_spot_core()->slug;?>_map_width_type]">
			<?php
			foreach(
				Array(
					''			=> esc_html__( "Full width", 'javospot' ),
					'boxed'	=> esc_html__( "Inner width", 'javospot' ),
				)
			as $strOption => $strLabel )
				printf(
					"<option value=\"{$strOption}\" %s>{$strLabel}</option>",
					selected( $jvfrm_spot_tso->get( jvfrm_spot_core()->slug . '_map_width_type', '' ) == $strOption, true, false )
				);
			?>
			</select>
		</fieldset>

		<?php if( function_exists( 'javo_spot_single' ) ) : ?>
			<h4><?php printf( esc_html__( "Single Listing - Layout Style", 'javospot' ), jvfrm_spot_core()->getSlug() ); ?></h4>
			<fieldset class="inner">
				<select name="jvfrm_spot_ts[lv_listing_single_type]">
					<?php
					$arrSingleCoreType = apply_filters(
						'jvfrm_spot_lv_listing_header',
						Array(
							'' => esc_html__( "Default Type", 'javospot' ),
							'type-a' => esc_html__( "Dynamic A", 'javospot' ),
							'type-b' => esc_html__( "Dynamic B", 'javospot' ),
							'type-grid' => esc_html__( "Grid Layout", 'javospot' ),
							'type-half' => esc_html__( "Half Layout", 'javospot' ),
							'type-left-tab' => esc_html__( "Left Tab Layout", 'javospot' ),
							'type-top-tab' => esc_html__( "Top Tab Layout", 'javospot' ),
						)
					);
					if( !empty( $arrSingleCoreType ) ) : foreach( $arrSingleCoreType as $strValue => $strLabel ) {
						printf(
							"<option value=\"{$strValue}\" %s>{$strLabel}</option>",
							selected( jvfrm_spot_tso()->get( 'lv_listing_single_type', 'type-grid' ) == $strValue, true, false )
						);
					} endif;
					?>
				</select>
			</fieldset>
		<?php endif; ?>

		<h4><?php esc_html_e( "Contact Form Settings", 'javospot' ); ?></h4>
		<fieldset class="inner">
			<label style="padding: 0 15px 0;">
				<input type="radio" name="jvfrm_spot_ts[single_listing_contact_type]" value='' <?php checked( '' == $jvfrm_spot_tso->get('single_listing_contact_type') );?>>
				<?php esc_html_e( "None", 'javospot');?>
			</label>
			<label style="padding: 0 15px 0;">
				<input type="radio" name="jvfrm_spot_ts[single_listing_contact_type]" value='contactform' <?php checked( 'contactform' == $jvfrm_spot_tso->get('single_listing_contact_type') );?>>
				<?php esc_html_e( "Contact Form", 'javospot');?>
			</label>
			<label style="padding: 0 15px 0;">
				<input type="radio" name="jvfrm_spot_ts[single_listing_contact_type]" value='ninjaform' <?php checked( 'ninjaform' == $jvfrm_spot_tso->get('single_listing_contact_type') );?>>
				<?php esc_html_e( "Ninja Form", 'javospot');?>
			</label>

		</fieldset>
		<fieldset class="inner">
			<label>
				<?php esc_html_e('Contact Form ID', 'javospot');?><br>
				<input type="text" name="jvfrm_spot_ts[single_listing_contact_form_id]" value="<?php echo esc_attr( $jvfrm_spot_tso->get('single_listing_contact_form_id' ) );?>">
			</label>
			<p><?php esc_html_e('To create a Contact Form ID, please go to the Contact Form Menu.', 'javospot');?></p>
		</fieldset>

	</td></tr><tr><th>
		<?php esc_html_e( "Mobile Search", 'javospot');?>
		<span class="description">	</span>
	</th><td>
		<h4><?php esc_html_e( "Search Result Map Page", 'javospot' ); ?></h4>
		<fieldset class="inner">
			<select name="jvfrm_spot_ts[lv_listing_mobile_search_requester]">
				<?php
				$arrTemplates	= apply_filters( 'jvfrm_spot_get_map_templates',
					Array( esc_html__( "Not Set", 'javospot' ) => '' )
				);
				foreach( $arrTemplates as $label => $value )
					printf( "<option value=\"{$value}\" %s>{$label}</option>",
						selected( $jvfrm_spot_tso->get( 'lv_listing_mobile_search_requester' ) == $value, true, false )
					);
				?>
			</select>
		</fieldset>

		<h4><?php esc_html_e( "Columns", 'javospot' ); ?></h4>
		<fieldset class="inner">
			<?php
			$arrColumns		= Array();
			for( $intCount=1; $intCount <= 3; $intCount++ )
				$arrColumns[]	= sprintf(
					"<option value=\"{$intCount}\" %s>{$intCount} %s</option>",
					selected( $jvfrm_spot_tso->get( 'lv_listing_mobile_search_columns' , 1) == $intCount, true, false ) ,
					_n( 'Column', 'Columns', $intCount, 'javospot' )
				);


			// Columns
			echo join( '\n',
				Array(
					'<select name="jvfrm_spot_ts[lv_listing_mobile_search_columns]">',
					join( '\n', $arrColumns ),
					'</select>',
				)
			);

			// Linked
			if( method_exists( 'jvfrm_spot_search1', 'fieldParameter' ) ) {
				$arrParamsArgs		= jvfrm_spot_search1::fieldParameter( null );
				$arrColumnFields		= isset( $arrParamsArgs[ 'value' ] ) ? $arrParamsArgs[ 'value' ] :Array();
				if( !empty( $arrColumnFields ) ) {
					for( $intCount=1; $intCount <= 3; $intCount++ ) {
						$strThisValue	= $jvfrm_spot_tso->get( 'lv_listing_mobile_search_column' . $intCount );
						printf( "
							<p>
								<label>%s {$intCount}</label>
								<select name=\"jvfrm_spot_ts[lv_listing_mobile_search_column{$intCount}]\">"
							, esc_html__( "Column", 'javospot' )
						);
							foreach( $arrColumnFields as $label => $key )
								printf( "<option value=\"{$key}\" %s>{$label}</option>",
									selected( $strThisValue == $key, true, false )
								);
							echo "</select>";
						echo "</p>";
					}
				}
			} ?>
		</fieldset>
	</td></tr>
	<?php if( function_exists( 'javo_spot_single' ) ) : ?>
		<tr><th>
			<label><?php _e( "Google Map Styles On Single Listing Pages", 'javospot' );?></label>
		</th>
		<td>
			<textarea name="jvfrm_spot_ts[map_style_json]" rows="5" style="width:100%;"><?php echo stripslashes( jvfrm_spot_tso()->get( 'map_style_json', null ) );?></textarea>
			<div>
				<?php
				printf(
					__( 'Please <a href="%1$s" target="_blank">click here</a> to create your own stlye and paste json code here.', 'javospot' ),
					esc_url( 'mapstyle.withgoogle.com' )
				); ?>
			</div>
			<div>
				<small>( <?php esc_html_e( "This code will overwritten map color setting", 'javospot' ); ?> )</small>
			</div>
		</td></tr>
	<?php endif; ?>
	<tr><th>
			<?php esc_html_e("Single Listing Color Settings", 'javospot');?>
			<span class="description">
				<?php esc_html_e("The color set up here will have the first priority to be applied.", 'javospot');?>
			</span>
		</th><td>
			<h4><?php esc_html_e("Backgound Color",'javospot'); ?></h4>
			<fieldset class="inner">
				<input name="jvfrm_spot_ts[single_page_background_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get('single_page_background_color','#f4f4f4') );?>" class="wp_color_picker" data-default-color="#f4f4f4">
			</fieldset>

			<h4><?php esc_html_e("Background Color (Box layout)",'javospot'); ?></h4>
			<fieldset class="inner">
				<input name="jvfrm_spot_ts[single_page_box_background_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get('single_page_box_background_color','#ffffff') );?>" class="wp_color_picker" data-default-color="#ffffff">
			</fieldset>

			<h4><?php esc_html_e("Title Color",'javospot'); ?></h4>
			<fieldset class="inner">
				<input name="jvfrm_spot_ts[single_page_title_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get('single_page_title_color','#666') );?>" class="wp_color_picker" data-default-color="">
			</fieldset>

			<h4><?php esc_html_e("Location Title Color",'javospot'); ?></h4>
			<fieldset class="inner">
				<input name="jvfrm_spot_ts[single_page_location_title_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get('single_page_location_title_color','#ffffff') );?>" class="wp_color_picker" data-default-color="">
			</fieldset>

			<h4><?php esc_html_e("Description Text Color",'javospot'); ?></h4>
			<fieldset class="inner">
				<input name="jvfrm_spot_ts[single_page_description_color]" type="text" value="<?php echo esc_attr( $jvfrm_spot_tso->get('single_page_description_color','#888888') );?>" class="wp_color_picker" data-default-color="#888888">
			</fieldset>
		</td>
		</tr>
	</table>
</div>