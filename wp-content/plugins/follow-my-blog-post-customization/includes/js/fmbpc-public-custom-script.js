jQuery(document).ready(function($) {
	
	$( document ).on( "click", ".message-followers-btn", function() {
		
		var id=$(this).attr('data-id');
		var title=$(this).attr('data-title');
		
		$('.modal-title span').text(title);
		
		$( '.message-post-id' ).val(id);
		$('#fmbpc_message_followed_modal').removeClass( 'fade' );
		$("#fmbpc_message_followed_modal").addClass('fade in').show();
	
	});
	$( document ).on( "click", ".close", function() {
		$('#fmbpc_message_followed_modal').hide();
	});
	$( document ).on( "click", ".javo-add-item-map-search-find", function() {
		$('.jv-item-input-address').val( $('.javo-add-item-map-search').val() );
	});
});