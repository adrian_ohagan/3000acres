<?php
/**
 * Plugin Name: Follow My Blog Post - Customization
 * Plugin URI: http://wpweb.co.in
 * Description: Follow My Blog Post Customzation plugin allows to customize follow my blog post plugin with javo wordpress theme.
 * Version: 1.0.0
 * Author: WPWeb
 * Author URI: http://wpweb.co.in  
 * Text Domain: fmbpc
 * Domain Path: languages
 * 
 * @package Follow My Blog Post - Customization
 * @category Core
 * @author WPWeb
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Basic plugin definitions 
 * 
 * @package Follow My Blog Post - Customization
 * @since 1.0.0
 */

if( !defined( 'FMBPC_DIR' ) ) {
	define( 'FMBPC_DIR', dirname( __FILE__ ) ); // plugin dir
}
if( !defined( 'FMBPC_URL' ) ) {
	define( 'FMBPC_URL', plugin_dir_url( __FILE__ ) ); // plugin url
}
if( !defined( 'JAVO_FOLLOW_SLUG' ) ) {
	define( 'JAVO_FOLLOW_SLUG', 'followed' ); // plugin url
}

// add action to load plugin
add_action( 'plugins_loaded', 'fmbpc_plugin_loaded' );

function fmbpc_plugin_loaded() {
	
	if( class_exists( 'Wpw_Fp_Model' ) ) {
		
		function fmbpc_template_include() {
			javo_dashboard::$pages['JAVO_FOLLOW_SLUG'] = 'followed';
		}
		add_action( 'init', 'fmbpc_template_include' );
		
		function fmbpc_add_follow_manu_page_template( $javo_db_sidemenus ) {
			
			$shopes_menu	= $javo_db_sidemenus['shop'];
			$shopes_menu[]	= Array(
				'li_class'		=> 'side-menu followed'
				, 'url'			=> home_url( JAVO_DEF_LANG . JAVO_MEMBER_SLUG . '/' . wp_get_current_user()->user_login . '/' . JAVO_FOLLOW_SLUG )
				, 'icon'		=> 'glyphicon glyphicon-cog'
				, 'label'		=> __("Sites i follow", 'javo_fr')
			);
			$javo_db_sidemenus['shop'] = $shopes_menu;
			
			return $javo_db_sidemenus;
		}
		add_filter( 'javo_db_sidemenus_listing', 'fmbpc_add_follow_manu_page_template' );
		
		function fmbpc_followed_list_template_url( $path, $page_slug ) {
			
			if( $page_slug == 'followed' ) {
				$path	= FMBPC_DIR . '/includes/templates/mypage-followed.php';
			}
			
			return $path;
		}
		add_filter( 'javo_dashboard_custom_template_url', 'fmbpc_followed_list_template_url', 10, 2 );
		
		function fmbpc_javo_after_shop_item() { ?>
			<button type="button" class="btn btn-circle btn-primary mypage-tooltips message-followers-btn wpweb-bg-blue-color" title="<?php _e('Message Followers', 'javo_fr');?>" data-title=" <?php echo get_the_title();?>" data-id="<?php the_ID();?>"><i class="glyphicon glyphicon-envelope"></i></button>
			<?php
		}
		add_action( 'javo_after_shop_item', 'fmbpc_javo_after_shop_item' );
		
		function fmbpc_end_shop_item_list() { ?>
		
			<div id='fmbpc_message_followed_modal' class="modal fade modal-popup" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php _e('Message Site Followers -', 'javo_fr');?><span>title</span></h4>
						</div>
			      <div class="modal-body">
			      <form method="post" id="message-followed-form">
			       <table>
			       <tr valign="top">
						<th scope="row">
							<label for="fmbpc_followed_email_subject"><?php _e( 'Email Subject:', 'wpwfp' ); ?></label>
						</th>
						<td>
							<input type="text" name="fmbpc_followed_email_subject" class="fmbpc_followed_email_subject" id="fmbpc_followed_email_subject" value="" size="76" required/></br>
							<span class="description"><?php _e( 'This is the subject of the email that will be sent to the followers', 'wpwfp' ); ?></span>
							<input type="hidden" name='fmbpc_post_id' value='' class='message-post-id'>
						</td>
					</tr>
			
					<!-- Email Body -->
					<tr valign="top" class="followed_email_body">
						<th scope="row">
							<label for="fmbpc_followed_email_body"><?php _e( 'Email Body:', 'wpwfp' ); ?><span class="fmbpc_email_error">*</span></label>
						</th>
						<td>
							<?php 
								$settings = array( 'teeny' => true );
								wp_editor( '', 'fmbpc_followed_email_body', $settings );
							?></br>
							<span class="description"><?php _e( 'This is the body, main content of the email that will be sent to the followers.', 'wpwfp' ); ?></span>
						
						</td>
					</tr>
				  </table>
			      </div>
			      <div class="modal-footer">
			      <input type="hidden" name="fmbpc_send_email" value="1" />
				  <input type="submit" name="fmbpc_send_email_submit" class="btn btn-primary btn-lg" value="Send">
			      </div>
			    </div>
			
			  </div>
			</div>
		
		<?php 
		}
		add_action( 'javo_end_shop_item_list', 'fmbpc_end_shop_item_list' );
		
		function fmbpc_custom_frontend_scripts() {
			wp_register_script( 'fmbpc-public-scripts',FMBPC_URL .'includes/js/fmbpc-public-custom-script.js', array('jquery' ) , '1.0.0', true );
			wp_enqueue_script( 'fmbpc-public-scripts' );
		}
		
		add_action( 'wp_enqueue_scripts', 'fmbpc_custom_frontend_scripts' );
		
		function fmbpc_send_email_followed() {
			
			global $wpw_fp_model;
			
			$prefix = WPW_FP_META_PREFIX;
			
			if( isset( $_POST['fmbpc_send_email_submit'] )) {
				
				$post_id		= isset($_POST['fmbpc_post_id']) ? $_POST['fmbpc_post_id'] : '';
				$email_subject 	= isset($_POST['fmbpc_followed_email_subject']) ? $_POST['fmbpc_followed_email_subject'] : '';
				$email_body 	= isset($_POST['fmbpc_followed_email_body']) ? $_POST['fmbpc_followed_email_body'] : '';
				
				$args = array();
				
				$args['postid'] 			= $post_id;
				$args['wpw_fp_status'] 		= 'subscribe';
				
				$data = $wpw_fp_model->wpw_fp_get_follow_post_users_data( $args );
				
				$followers_count = count( $data );
				
				foreach ($data as $key => $value) {
					
					$user_email = get_post_meta( $value['ID'], $prefix.'post_user_email', true );
					
					if( !empty( $value['post_author'] ) ) {
						$user_email = $wpw_fp_model->wpw_fp_get_user_email_from_id( $value['post_author'] );
					}
					
					$sentemail = $wpw_fp_model->wpw_fp_send_email( $user_email, $email_subject, $email_body );
				}
			}
		}
		add_action ( 'init','fmbpc_send_email_followed');
		
		function fmbpc_remove_fillow_button_filter( $content ) {
			
			global $wpw_fp_public;
			
			if( has_filter( 'the_content', array( $wpw_fp_public, 'wpw_fp_follow_content_filter' ) ) ) {
				remove_filter( 'the_content', array( $wpw_fp_public, 'wpw_fp_follow_content_filter' ) ); // if this filter got priority different from 10 (default), you need to specify it
			}
			
			return $content;
		}
		add_filter( 'get_the_excerpt', 'fmbpc_remove_fillow_button_filter', 9 );
		
		function fmbpc_get_terms_filter ( $taxonomy, $post_id = 0, $input_name="", $parent = 0 ) {
			
			$javo_has_terms = wp_get_post_terms( $post_id, $taxonomy , Array( 'fields' => 'ids') );
			$javo_all_terms = get_terms( $taxonomy, Array( 'hide_empty' => false, 'parent' => $parent ));

			if( is_wp_error( $javo_has_terms ) || is_wp_error( $javo_all_terms ) )
			{
				return false;
			}

			ob_start();
			echo "<select class='fmbpc-term-list form-control' name='".$input_name."'>";
			foreach( $javo_all_terms as $term ) {
				echo "<option value='".$term->term_id."'";
				if( in_array( $term->term_id, $javo_has_terms ) ){ echo "selected"; }
				echo ">". $term->name . "</option>";
			}
			echo "</select>";
			
			$content = ob_get_clean();
			return $content;
		}
		
		// Filter to convert Local Council checkbox list to dropdown
		add_filter( 'javo_add_item_get_terms_list', 'fmbpc_get_terms_filter', 10, 4 );
	}
}