<?php 

/**
 * Template For Follow Post Register User Content
 * 
 * Handles to return design follow post register user content
 * 
 * Override this template by copying it to yourtheme/follow-my-blog-post/follow-post/user.php
 *
 * @package Follow My Blog Post
 * @since 1.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
	<?php 
		if( !empty( $follow_pos_class ) ) {
			echo "<div class='{$follow_pos_class}'>";
		}
	?>
	<?php
		echo apply_filters(
			'wpw_fp_follow_post_button',
			'<button type="button" class="wpw-fp-follow-btn wpw-fp-button wpw_fp_left '.$follow_class.'" data-status="'.$follow_status.'" data-postid="'.$post_id.'" data-current-postid="'.$current_post_id.'" data-follow-text="'.$follow_text.'" data-following-text="'.$following_text.'" data-unfollow-text="'.$unfollow_text.'" >
				<span class="wpw-following-text">'.$follow_label.'</span>
				<span class="wpw-unfollowing-text wpw-fp-display-none">'.$unfollow_text.'</span>
				<span class="wpw_fp_follow_loader"><img src="'.WPW_FP_IMG_URL.'/ajax-loader.gif" alt="..." /></span>
			</button>', $follow_class, $follow_status, $post_id, $current_post_id, $follow_text, $following_text, $unfollow_text, $follow_label
		);
	?>		
						
		<?php
			
			// Check follow message is not empty from meta or settings
			if( !empty( $follow_message ) ) {
				
				do_action( 'wpw_fp_follow_post_count_box', $follow_message, $post_id );
				
			}
		?>
		<?php 
		
			do_action( 'wpw_fp_follow_after_post_count_box', $follow_class, $follow_status, $post_id, $current_post_id, $follow_text, $following_text, $unfollow_text, $follow_label );
		?>
	<?php 
		if( !empty( $follow_pos_class ) ) {
			echo "</div>";
		}
	?>
	<div class='wpw_fp_clear'></div>