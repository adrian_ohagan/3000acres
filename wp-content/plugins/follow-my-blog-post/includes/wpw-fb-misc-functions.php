<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Misc Functions
 * 
 * All misc functions handles to
 * different functions
 * 
 * @package Follow My Blog Post
 * @since 1.1.0
 */

/**
 * Get Followers Count for Post / Page / Custom Post Types
 * 
 * Handles to get followers count of
 * post / page / custom post type by post id
 * 
 * @package Follow My Blog Post
 * @since 1.1.0
 */
function wpw_fp_get_post_followers_count( $post_id ) {

	//check if post id empty then return zero
	if( empty( $post_id ) ) { return 0; }

	$prefix = WPW_FP_META_PREFIX;

	//arguments to collect followers data by post
	$args = array( 
					'post_status'	=>	'publish',
					'post_parent' 	=>	$post_id,
					'posts_per_page'=>	'-1',
					'post_type' 	=>	WPW_FP_POST_TYPE,
					'meta_key'		=>	$prefix.'follow_status',
					'meta_value'	=>	'1'
				);

	//get data for post followed by users
	$data = get_posts( $args );

	//get followers count
	$counts = count( $data );

	//return followers count
	return apply_filters( 'wpw_fp_get_post_followers_count', $counts, $post_id );
}

/**
 * Get Taxonomy Terms Followers Count
 * 
 * Handles to get followers count of term by term id
 * 
 * @package Follow My Blog Post
 * @since 1.1.0
 */
function wpw_fp_get_term_followers_count( $term_id ) {

	//check if term id empty then return zero
	if( empty( $term_id ) ) { return 0; }

	$prefix = WPW_FP_META_PREFIX;

	//arguments to collect followers data by term
	$args = array( 
					'post_status'	=>	'publish',
					'post_type' 	=>	WPW_FP_TERM_POST_TYPE,
					'post_parent' 	=>	$term_id,
					'posts_per_page'=>	'-1',
					'meta_key'		=>	$prefix.'follow_status',
					'meta_value'	=>	'1'
				);

	//get data for term followed by users
	$data = get_posts( $args );

	//get followers count
	$counts = count( $data );

	return apply_filters( 'wpw_fp_get_term_followers_count', $counts, $term_id );
}

/**
 * Get Authors Followers Count
 * 
 * Handles to get followers count of author by author id
 * 
 * @package Follow My Blog Post
 * @since 1.4.0
 */
function wpw_fp_get_author_followers_count( $author_id ) {

	//check if author id empty then return zero
	if( empty( $author_id ) ) { return 0; }

	$prefix = WPW_FP_META_PREFIX;

	//arguments to collect followers data by author
	$args = array( 
					'post_status'	=>	'publish',
					'post_type' 	=>	WPW_FP_AUTHOR_POST_TYPE,
					'post_parent' 	=>	$author_id,
					'posts_per_page'=>	'-1',
					'meta_key'		=>	$prefix.'follow_status',
					'meta_value'	=>	'1'
				);

	//get data for author followed by users
	$data = get_posts( $args );

	//get followers count
	$counts = count( $data );

	return apply_filters( 'wpw_fp_get_author_followers_count', $counts, $author_id );
}

/**
 * Get Unsubscribe message
 * 
 * Handles to get get unsubscibe message and return
 * unsubscibe message html
 * 
 * @package Follow My Blog Post
 * @since 1.6.2
 */
function wpw_fp_get_unsubscribe_message() {

	global $wpw_fp_options;

	$unsubscribe_message = ''; // initialize its with blank

	// Check enable unsubscribe url & unsubscribe page is exist & unsubscribe message is not empty
	if( isset( $wpw_fp_options['enable_unsubscribe_url'] ) && $wpw_fp_options['enable_unsubscribe_url'] == '1'
		&& isset( $wpw_fp_options['unsubscribe_page'] ) && !empty( $wpw_fp_options['unsubscribe_page'] )
		&& isset( $wpw_fp_options['unsubscribe_message'] ) && !empty( $wpw_fp_options['unsubscribe_message'] ) ) {

		// get unsubscibe message
		$unsubscribe_message = $wpw_fp_options['unsubscribe_message'];

		// get url of unsubscribe page
		$url = get_permalink( $wpw_fp_options['unsubscribe_page'] );

		// make unsubscibe url
		$unsubscribe_url = '<a target="_blank" href="'.$url.'" >'.__( 'Unsubscribe', 'wpwfp' ).'</a>';

		// replace {unsubscribe url} with unsubscibe message
		$unsubscribe_message = str_replace( '{unsubscribe_url}', $unsubscribe_url, $unsubscribe_message );
	}

	return apply_filters( 'wpw_fp_get_unsubscribe_message', $unsubscribe_message );
}

/**
 * Check Extra Security
 * 
 * Handles to check extra security
 * 
 * @package Follow My Blog Post
 * @since 1.7.2
 */
function wpw_fp_extra_security( $post_id ) {
	
	$extra_security	= false;
	
	if( ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] ) ) {
		$extra_security	= true;
	}
	
	// Get Settings
	$wpw_fp_options = get_option( 'wpw_fp_options' );
	// If setting for autopost_thirdparty_plugins is set than make $extra_security compulsory false
	if( ( isset( $wpw_fp_options['autopost_thirdparty_plugins'] ) && $wpw_fp_options['autopost_thirdparty_plugins'] == 1 ) ) {
		
		$extra_security	= false;
	}
	
	$extra_security = apply_filters( 'wpw_fp_extra_security', $extra_security, $post_id );
	
	return $extra_security;
}