<div class="form-field">
	<img style="max-width:80%;"><br>
	<input type="hidden" name="lava_listing_category_marker" id="lava_listing_category_marker">
	<p class="description"><?php _e( "Category markers : you need to refresh map data after you upload or change pin (map marker). Listings >> Settings", 'Lavacode');?></p>
	<button type="button" class="button button-primary fileupload" data-featured-field="[name='lava_listing_category_marker']" data-result-src><?php _e('Change', 'Lavacode');?></button>
	<button type="button" class="button fileupload-remove"><?php _e( 'Remove' , 'Lavacode');?></button>
</div>

<div class="form-field">
	<label for="lava_listing_category_featured"><?php _e('Category Featured Image', 'Lavacode');?></label>
	<img style="max-width:80%;"><br>
	<input type="hidden" name="lava_listing_category_featured" id="lava_listing_category_featured">
	<button type="button" class="button button-primary fileupload" data-featured-field="[name='lava_listing_category_featured']"><?php _e('Change', 'Lavacode');?></button>
	<button type="button" class="button fileupload-remove"><?php _e( 'Remove' , 'Lavacode');?></button>
</div>
<?php
do_action('lava_file_script');