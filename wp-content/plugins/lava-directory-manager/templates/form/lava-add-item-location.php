<div class="form-inner">
	<label class="field-title"><?php _e( "Location", 'Lavacode' ); ?></label>
	<div class="address-group">
		<input class="lava-add-item-map-search" placeholder="<?php _e("Type an Address","Lavacode");?>">
		<input type="button" value="<?php _e('Find','Lavacode'); ?>" class="lava-add-item-map-search-find">
	</div>
</div>

<div class="map_area"></div>
<input type="hidden" name="lava_location[country]" value="<?php echo $edit->country; ?>" readonly>
<input type="hidden" name="lava_location[locality]" value="<?php echo $edit->locality; ?>" readonly>
<input type="hidden" name="lava_location[political]" value="<?php echo $edit->political; ?>" readonly>
<input type="hidden" name="lava_location[political2]" value="<?php echo $edit->political2; ?>" readonly>
<input type="hidden" name="lava_location[lat]" class="only-number" value="<?php echo $edit->lat; ?>" readonly>
<input type="hidden" name="lava_location[lng]" class="only-number" value="<?php echo $edit->lng; ?>" readonly>
<div class="form-inner">
	<label class="field-title"><?php _e( "Streetview Setting", 'Lavacode' ); ?></label>
	<input type="hidden" name="lava_location[street_visible]" value="0">
	<label>
		<input type="checkbox" name="lava_location[street_visible]" class='lava-add-item-set-streetview' value="1" <?php checked( $edit->street_visible == 1 ); ?>>
		<?php _e( "Show Streeview", 'Lavacode' ); ?>
	</label>
</div>
<div class="lava_map_advanced hidden">
	<div class="map_area_streetview"></div>
	<div class="form-inner">
		<label class="field-title"><?php _e( "Streetview Lat", 'Lavacode' ); ?></label>
		<input type="text" name="lava_location[street_lat]" value="<?php echo $edit->street_lat; ?>">
	</div>

	<div class="form-inner">
		<label class="field-title"><?php _e( "Streetview Lng", 'Lavacode' ); ?></label>
		<input type="text" name="lava_location[street_lng]" value="<?php echo $edit->street_lng; ?>">
	</div>

	<div class="form-inner">
		<label class="field-title"><?php _e( "POV: Heading", 'Lavacode' ); ?></label>
		<input type="text" name="lava_location[street_heading]" value="<?php echo $edit->street_heading; ?>">
	</div>

	<div class="form-inner">
		<label class="field-title"><?php _e( "POV: Pitch", 'Lavacode' ); ?></label>
		<input type="text" name="lava_location[street_pitch]" value="<?php echo $edit->street_pitch; ?>">
	</div>

	<div class="form-inner">
		<label class="field-title"><?php _e( "POV : Zoom", 'Lavacode' ); ?></label>
		<input type="text" name="lava_location[street_zoom]" value="<?php echo $edit->street_zoom; ?>">
	</div>
</div>
