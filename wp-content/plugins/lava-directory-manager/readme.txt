=== Lava Directory Manager ===
Contributors: lavacode
Requires at least: 3.2
Tested up to: 4.7
Stable tag: 1.0.9
Tags:  addresses, address book, addressbook, bio, bios, biographies, business, businesses, business directory, business-directory, business directory plugin, directory widget, chamber of commerce, church, contact, contacts, connect, connections, directory, directories, hcalendar, hcard, ical, icalendar, image, images, list, lists, listings, member directory, members directory, members directories, microformat, microformats, page, pages, people, profile, profiles, post, posts, plugin, shortcode, staff, user, users, vcard, wordpress business directory, wordpress directory, wordpress directory plugin, wordpress business directory, wordpress local directory plugin
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Manage Directory listings. Provided front-end form, google map support, listing, filtering.

== Description ==

Lava directory manager plugin is for business listings

This plugin enables you to use the power and flexibility of WordPress to generate optimized pages for your business listings.

Intro page : http://lava-code.com/directory/

Documentation : http://lava-code.com/directory/documentation/

Demo site : http://lava-code.com/directory-demo/


== Installation ==

Install via Search:

1. In WordPress admin, visit Plugins > Add New
2. Search for "lava directory manager"
3. Click the "Install Now" link.
4. Once installed, click the "Activate Plugin" link.
5. Visit settings to setup pages

Install via Upload:

1. In WordPress admin, visit "Plugins" > "Add New" > "Upload"
2. Upload lava-directory-manager.zip file
3. Once uploaded, click the "Activate Plugin" link
4. Visit settings to setup pages


That's it!
If you need more documentation, please visit here ( http://lava-code.com/directory/documentation/ )


== Frequently Asked Questions ==

= Can I use my existing WordPress theme? =

Yes! Lava directory manager works out-of-the-box with nearly every WordPress theme.

= Will this work on WordPress multisite? =

Yes! If your WordPress installation has multisite enabled, Lava directory manager will work on it.

= Where can I get support? =

We will open our own support site soon. or you can create tickets on wordpress plugin support page.

= Where can I find documentation? =

Our codex can be found at <a href="http://lava-code.com/directory/documentation/">http://lava-code.com/directory/documentation/</a>.

= Where can I report a bug? =

Report bugs, suggest ideas, and participate in development at wp.lava.code@gmail.com .

== Screenshots ==
1. ** Setting page ** - Lava directory manager plugin setting page.
2. ** Back-end form ** - You can manager (add/edit/delete) property information.
3. ** Front-end form ** - Your user can add properties on front-end


== Languages ==

It is currently (v0.1) available only English version. we are happy to have volunteer to assist to translate in your own languages.

https://www.transifex.com/projects/p/lava-directory-manager/



Please email us at wp.lava.code@gmail.com , if you are interested.
Thank you in advance.


== Upgrade Notice ==

= 1.0.9 =
*Release Date - 21th April, 2017*

= 1.0.8 =
*Release Date - 7th Mar, 2017*

= 1.0.7 =
*Release Date - 14th Feb, 2017*

= 1.0.6 =
*Release Date - 02nd Jan, 2017*

= 1.0.5 =
*Release Date - 07th Dec, 2016*

= 1.0.4.1 =
*Release Date - 12th Sep, 2016*

= 1.0.3 =
*Release Date - 2nd Aug, 2016*

= 1.0.2.2 =
*Release Date - 28th Jun, 2016*

= 1.0.2.1 =
*Release Date - 16th Jun, 2016*

= 1.0.2 =
*Release Date - 16th Jun, 2016*

= 1.0.1.1 =
*Release Date - 20th Mar, 2016*

= 1.0.0 =
*Release Date - 14th Mar, 2016*

= 0.1.1.5 =
*Release Date - 3th Mar, 2016*

= 0.1.1.3 =
*Release Date - 28th Jan, 2016*

= 0.1.1 =
*Release Date - 28th Jan, 2016*

= 0.1.0 =
*Release Date - 22nd Jan, 2015*

= 0.0.1 =
*Release Date - 30th Jun, 2015*


== Changelog ==

= 1.0.9 =
Improved : Shortcode > add Form > Style Improved
Improved : Shortcode > Listing > Style Improved
Improved : Shortcode > add form : Not logged-in user auto register
Improved : Shortcode > add form : Added user pwd
Added : Single > Added Edit Button
Added : Single > Added Publish Button & Preview Mode

= 1.0.8 =
* Improved : Dashboard shortcode > added a tab for additional hooks

= 1.0.7 =
* Added : Admin > Listings > Settings > Added Cross domain option
* Fixed : Admin > Listings > Quick Edit : Detail image issue
* Fixed : get_json fuction > Security issue fixed
* Fixed : Double line code fixed

= 1.0.6 =
* Improved : Social links field translate
* Improved : Upload listing amenities icon
* Added : Show default / custom amenities icon on single listing page
* Improved : Compatibility work for Addons.
* Improved : Minor css, js

= 1.0.5 =
* Added : Wordpress 4.7 compatibility.
* Added : Option to change slug. "Listing" to your own name.
* Added : Edit Listing > Detail images Multi-upload.
* Improved : Display taxonomies. 'show admin column => true'
* Added : Amenities > Edit taxonomy > 'icon' field added.
* Added : SNS social link on listing
* Fixed : My Dashboard Shortcode > get_current_userinfo notice issue.
* Improved : Change word(item >> listing)
* Fixed : Single listing > streetview panorama radius issue.
* Improve : edit listing > preview featured image.
* Added : edit listing > added remove featured image checkbox.
* Added options title, button names on Contact Widget for Javo themes
* Improve : Edit page > page template option issue (WP 4.7)

= 1.0.4.1 =
* Added : Wordpress 4.6.1 compatibility.
* Improved : Generator > Json big data & Added progress bar. it's for when you have huge data.
* Improved : Some instruction (ex. Listing Category pages)
* Improved : Image uploading dimention tooltip.
* Improved : Video instruction for video addon.

= 1.0.3 =
* Improved / Fixed : WPML compatibility core
* Improved : Pot file
* Improved : Minor translation string

= 1.0.2.2 =
* Added : Google Map API Key field.

= 1.0.2.1 =
* Added : WP compatibility 4.5.2.

= 1.0.2 =
* Added : Cross domain option.
* Improved : Licence key for addon updates.
* Improved : Undefined function error for themes
* Fixed : Update button bug ( from WP 4.5 )
* Improved / Fixed : minor js, css

= 1.0.1.1 =
* Added : a button for licence key instruction.
* Fixed : addons auto update compare issue.
* Improved : WordPress newest version compatible (4.4.1).

= 1.0.0 =
* Added : addons auto update
* Fixed : Minor css, js issues

= 0.1.1.5 =
* Fixed : Minor css, js issue

= 0.1.1.3 =
* Fixed : A wrong required. 0.1.1

= 0.1.1 =
* fixed : request class-addons.php

= 0.1.0 =
* Improved : WordPress newest version compatible (4.4.1).
* Improved : Better post type for unique and common.
* Improved : CSS, JS

= 0.0.1 =
*Release Date - 30th Jun, 2015*

* Initial release