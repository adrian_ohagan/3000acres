<?php
if (!function_exists ('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}
class jvfrm_spot_Import {

	const REMOTE_SITE = 'http://wpjavo.com/demo/';
	public $name = '';
	public $prefix = '';
	public $dirFiles = '';
    public $message = "";
    public $attachments = false;
	public static $hInstance =null;

    function __construct() {
		$this->setVariables();
		$this->register_hooks();
        add_action('admin_menu', array(&$this, 'jvfrm_spot_admin_import'));
    }

	function setVariables() {
		$this->name = $this->name = jvfrm_spot_admin_helper::$instance->name;
		$this->prefix = sanitize_title( $this->name );
		$this->dirFiles = sprintf( '%s/files', javoSpotCore()->import_path );		
	}

	function register_hooks() {
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue' ) );
	}

	public function admin_enqueue() {
		?>
		<style type="text/css">
			#jv-metaboxes-general{ letter-spacing:1px; }
			#jv-metaboxes-general .text-uppercase{ text-transform:uppercase; }
			#jv-metaboxes-general .jv-main-box-wrap{ border:solid 1px #ddd; background-color:#fff; padding:20px; }
			#jv-metaboxes-general ul.ul-li-letter-spacing li{ letter-spacing:1px; }
			#jv-metaboxes-general .big-font{ 
				display:block;
				margin:20px 0;
				font-size:1.2em;
			}
			#jv-metaboxes-general .normal-font{ font-size:1em; }
			#jv-metaboxes-general .jvfrm-section-content{ margin:10px; padding:10px; }			
			#jv-metaboxes-general input[type="submit"][name="import"]{
				line-height:1;
				padding:10px 20px;
				height:auto;
				font-size:1.5em;
				letter-spacing:1px;
				text-transform:uppercase;
			}
			#jv-metaboxes-general .import_load{			
				margin:0;
				margin-top:10px;
			}
			#jv-metaboxes-general .jv-progress-bar-wrapper{
				display:block;
				margin:10px;
			}
			#jv-metaboxes-general .jv-progress-bar-wrapper > .progress-bar-wrapper,
			#jv-metaboxes-general .jv-progress-bar-wrapper > .progress-value{
				display:inline-block;
				vertical-align:middle;
			}
		</style>
		<?php
	}

    function init_jvfrm_spot_import() {
        if(isset($_REQUEST['import_option'])) {
            $import_option = $_REQUEST['import_option'];
            if($import_option == 'content'){
                $this->import_content('proya_content.xml');
            }elseif($import_option == 'custom_sidebars') {
                $this->import_custom_sidebars('custom_sidebars.txt');
            } elseif($import_option == 'widgets') {
                $this->import_widgets('widgets.txt','custom_sidebars.txt');
            } elseif($import_option == 'options'){
                $this->import_options('jvfrm_spot_themes_settings.txt');
            }elseif($import_option == 'menus'){
                $this->import_menus('menus.txt');
            }elseif($import_option == 'settingpages'){
                $this->import_settings_pages('settingpages.txt');
            }elseif($import_option == 'complete_content'){
                $this->import_content('proya_content.xml');
                $this->import_options('jvfrm_spot_themes_settings.txt');
                $this->import_widgets('widgets.txt','custom_sidebars.txt');
                $this->import_menus('menus.txt');
                $this->import_settings_pages('settingpages.txt');
                $this->message = __("Content imported successfully", 'javo');
            }
        }
    }

    public function import_content($file){
            ob_start();

            require_once( $this->dirFiles . '/../class.wordpress-importer.php' );			
            $jvfrm_spot_import = new WP_Import();
            set_time_limit(0);
            $jvfrm_spot_import->fetch_attachments = $this->attachments;
			
            $returned_value = $jvfrm_spot_import->import( $this->dirFiles . '/' . $file );
            if(is_wp_error($returned_value)){
                $this->message = __("An Error Occurred During Import", 'javo');
            }
            else {
                $this->message = __("Content imported successfully", 'javo');
            }
            ob_get_clean();
    }

    public function import_widgets($file, $file2){
        $this->import_custom_sidebars($file2);
        $options = $this->file_options($file);
        foreach ((array) $options['widgets'] as $jvfrm_spot_widget_id => $jvfrm_spot_widget_data) {
            update_option( 'widget_' . $jvfrm_spot_widget_id, $jvfrm_spot_widget_data );
        }
        $this->import_sidebars_widgets($file);
        $this->message = __("Widgets imported successfully", 'javo');
    }

    public function import_sidebars_widgets($file){
        $jvfrm_spot_sidebars = get_option("sidebars_widgets");
        unset($jvfrm_spot_sidebars['array_version']);
        $data = $this->file_options($file);
        if ( is_array($data['sidebars']) ) {
            $jvfrm_spot_sidebars = array_merge( (array) $jvfrm_spot_sidebars, (array) $data['sidebars'] );
            unset($jvfrm_spot_sidebars['wp_inactive_widgets']);
            $jvfrm_spot_sidebars = array_merge(array('wp_inactive_widgets' => array()), $jvfrm_spot_sidebars);
            $jvfrm_spot_sidebars['array_version'] = 2;
            wp_set_sidebars_widgets($jvfrm_spot_sidebars);
        }
    }

    public function import_custom_sidebars($file){
        $options = $this->file_options($file);
        update_option( 'jvfrm_spot_sidebars', $options);
        $this->message = __("Custom sidebars imported successfully", 'javo');
    }

    public function import_options($file){
        $options = $this->file_options($file);
        update_option( 'jvfrm_spot_themes_settings', $options);
        $this->message = __("Options imported successfully", 'javo');
    }

    public function import_menus($file){
        global $wpdb;
        $jvfrm_spot_terms_table = $wpdb->prefix . "terms";
        $this->menus_data = $this->file_options($file);
        $menu_array = array();
        foreach ($this->menus_data as $registered_menu => $menu_slug) {
            $term_rows = $wpdb->get_results("SELECT * FROM $jvfrm_spot_terms_table where slug='{$menu_slug}'", ARRAY_A);
            if(isset($term_rows[0]['term_id'])) {
                $term_id_by_slug = $term_rows[0]['term_id'];
            } else {
                $term_id_by_slug = null;
            }
            $menu_array[$registered_menu] = $term_id_by_slug;
        }
        set_theme_mod('nav_menu_locations', array_map('absint', $menu_array ) );

    }
    public function import_settings_pages($file){
        $pages = $this->file_options($file);

        foreach($pages as $jvfrm_spot_page_option => $jvfrm_spot_page_id){
            update_option( $jvfrm_spot_page_option, $jvfrm_spot_page_id);
        }
    }
    public function file_options($file){		
        $file_content = $this->get_contents( $file );
        if ($file_content) {
            $unserialized_content = unserialize(base64_decode($file_content));
            if ($unserialized_content) {
                return $unserialized_content;
            }
        }
        return false;
    }

    function get_contents( $path='' ) {
		return wp_remote_retrieve_body( wp_remote_get( self::REMOTE_SITE . $path ) );
    }

    function jvfrm_spot_admin_import() {		
		 $this->pagehook = add_submenu_page(
			sanitize_title( $this->name ),
			esc_html__( "Javo Import", 'javo' ),
			esc_html__( "Demo Import", 'javo' ),
			'manage_options',
			sanitize_title( $this->name . '_import' ),
			array(&$this, 'jvfrm_spot_generate_import_page'),
			'dashicons-download'
		);
    }

    function jvfrm_spot_generate_import_page() {
		do_action( 'jvfrm_spot_admin_helper_page_header' );
		do_action( 'jvfrm_spot_admin_helper_import_header' );
		?>		
        <div id="jv-metaboxes-general" class="wrap jvfrm-page jvfrm-page-info">
            <h2 class="jvfrm-page-title text-uppercase"><?php _e('Javo One-Click Import', 'javo') ?></h2>
            <form method="post" action="" id="importContentForm">
                <div class="jvfrm-page-form">
					<p class="" style="background:#f4f4f4; border:solid 1px #ccc; padding:10px; max-width:700px; width:100%;">
						<?php
						printf(
							'%1$s<a href="%6$s" target="_blank"><strong>%3$s</strong></a> (<a href="%8$s" target="_blank">%5$s</a>)<br>
							%2$s<a href="%7$s" target="_blank"><strong>%4$s</strong></a> (<a href="%9$s" target="_blank">%5$s</a>)',
							esc_html__( 'Please make sure that you need to activate "Required plugins".', 'javo' ),							
							esc_html__( 'Please enable "Optimized CSS, Optimized JS" on ultimate addons (plugin required).', 'javo' ),							
							esc_html__( 'Go to check', 'javo' ),
							esc_html__( 'Go to enable', 'javo' ),
							esc_html__( 'Tutorial', 'javo' ),
							admin_url( 'admin.php?page=' . $this->prefix . '_plugins' ),
							admin_url( 'admin.php?page=ultimate-dashboard#css-settings' ),
							esc_url( 'directory-docs.wpjavo.com/plugin-install/' ),
							esc_url( 'directory-docs.wpjavo.com/ultimate-vc-addons-optimized-css-js/' )
						); ?>
					</p>
                    <div class="jvfrm-page-form-section-holder clearfix jv-main-box-wrap">
                        <h3 class="jvfrm-page-section-title text-uppercase">Import Demo Content</h3>
                        <div class="jvfrm-page-form-section">
                            <div class="jvfrm-field-desc">
                                <h4 class="text-uppercase"><?php esc_html_e('Demo Site', 'javo'); ?></h4>
                                <p>Choose demo site you want to import</p>
                            </div>
                            <div class="jvfrm-section-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <select name="import_example" id="import_example" class="form-control jvfrm-form-element">
												<option value="javo-demo">Demo 1</option>
												<option value="javo-demo2">Demo 2</option>
												<option value="javo-demo3">Demo 3</option>
												<option value="javo-demo4">Demo 4</option>
												<option value="javo-demo5">Demo 5</option>
												<option value="javo-demo6">Demo 6</option>
												<option value="javo-demo7">Demo 7</option>
												<option value="javo-demo8">Demo 8</option>
												<option value="javo-demo9">Demo 9</option>
												<option value="javo-demo10">Demo 10</option>
												<option value="javo-demo11">Demo 11</option>
												<option value="javo-demo12">Demo 12</option>
												<option value="javo-demo13">Demo 13</option>
												<option value="javo-demo14">Demo 14</option>
												<option value="javo-demo15">Demo 15</option>
												<option value="javo-demo16">Demo 16</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row next-row">
                                        <div class="col-lg-3">
                                        	<img id="demo_site_img" src="<?php echo get_stylesheet_directory_uri() . '/screenshot.png' ?>" width="250">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="jvfrm-page-form-section" >
                            <div class="jvfrm-section-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-3">
											<?php esc_html_e( "Import Type", 'javo' ); ?>
                                            <select name="import_option" id="import_option" class="form-control jvfrm-form-element">
                                                <option value="">Please Select</option>
                                                <option value="complete_content">All</option>
                                                <option value="content">Content</option>
                                                <option value="widgets">Widgets</option>
                                                <option value="menus">Menus</option>
                                                <option value="options">Options</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="jvfrm-page-form-section" >
                            <div class="jvfrm-field-desc">
                                <h4 class="text-uppercase"><?php esc_html_e('Import attachments', 'javo'); ?></h4>
                            </div>

                            <div class="jvfrm-section-content">
								<label>
									<input type="checkbox" value="1" class="jvfrm-form-element" name="import_attachments" id="import_attachments">
									Do you want to import media files?
								</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-button-section clearfix">
                                    <input type="submit" class="button button-primary" value="Import" name="import" id="import_demo_data" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3"></div>

                        </div>

                        <div class="import_load">
							<span><?php _e('The import process may take some time. Please be patient.', 'javo') ?> </span><br />
                            <div class="jv-progress-bar-wrapper html5-progress-bar">
                                <div class="progress-bar-wrapper">
                                    <progress id="progressbar" value="0" max="100"></progress>
                                </div>
                                <div class="progress-value">0%</div>
                                <div class="progress-bar-message"></div>
                            </div>
                        </div>
                        <div class="alert alert-warning" style="border:solid 1px #ddd; background-color:#f4f4f4; padding:10px;;">
                            <strong class="text-uppercase big-font">Important notes:</strong>
                            <ul class="ul-li-letter-spacing ul-li-font-size-1-2">
                                <li>Please note that import process will take time needed to download all attachments from demo web site.</li>
								<li> Please make sure that you have installed all of plugins you need.</li>
								<li class="warning">
									<ul>
										<li> <h4 class="text-uppercase big-font">Tips, If you have problems with Import.</h4></li>
										<li class="normal-font"> 1. Cause : Low uploading size (a common cause. 99%). Solution : Increase your uploading size / memory in your server.  <a href="http://directory-docs.wpjavo.com/import-problem/" target="_blank">( Details )</a></li>
										<li class="normal-font"> 2. Solution : Deactivate Visual Composer plugin during import demo data. few servers are not importing visual composer (5.0+) <a href="<?php echo admin_url( 'plugins.php' ); ?>" target="_blank">( Go to Deactivate )</a></li>
										<li class="normal-font"> 3. Solution : Import demo xml files directly. <a href="http://directory-docs.wpjavo.com/how-to-import-demo-data-manually-directly/" target="_blank">( Details )</a></li>
										<li class="normal-font"> If you still have problems, ask Javo support team to install a demo data. <a href="http://javothemes.com/forum/free-installation" target="_blank">( Details )</a></li>
									</ul>								
								</li>
                            </ul>
                        </div>
                </div>
            </form>
        </div>
        <script type="text/javascript">
			var $j = window.jQuery;
            $j(document).ready(function() {
				/**
				$j('#import_example').on('change', function (e) {
					var optionSelected = $j("option:selected", this).val();
					$j('#demo_site_img').attr('src', '<?php echo get_template_directory_uri() . '/css/admin/images/demos/' ?>' + optionSelected + '.jpg' );
				}); */

                $j(document).on('click', '#import_demo_data', function(e) {
                    e.preventDefault();
                    if ($j( "#import_option" ).val() == "") {
                    	alert('Please select Import Type.');
                    	return false;
                    }
                    if (confirm('Are you sure, you want to import Demo Data now?')) {
                        $j('.import_load').css('display','block');
                        var progressbar = $j('#progressbar')
                        var import_opt = $j( "#import_option" ).val();
                        var import_expl = $j( "#import_example" ).val();
                        var p = 0;
                        if(import_opt == 'content'){
                            for(var i=1;i<10;i++){
                                var str;
                                if (i < 10) str = 'jvfrm_spot_demo_content_0'+i+'.xml';
                                else str = 'jvfrm_spot_demo_content_'+i+'.xml';
                                jQuery.ajax({
                                    type: 'POST',
                                    url: ajaxurl,
                                    data: {
                                        action: 'jvfrm_spot_dataImport',
                                        xml: str,
                                        example: import_expl,
                                        import_attachments: ($j("#import_attachments").is(':checked') ? 1 : 0)
                                    },
                                    success: function(data, textStatus, XMLHttpRequest){
                                        p+= 10;
                                        $j('.progress-value').html((p) + '%');
                                        progressbar.val(p);
                                        if (p == 90) {
                                            str = 'jvfrm_spot_demo_content_10.xml';
                                            jQuery.ajax({
                                                type: 'POST',
                                                url: ajaxurl,
                                                data: {
                                                    action: 'jvfrm_spot_dataImport',
                                                    xml: str,
                                                    example: import_expl,
                                                    import_attachments: ($j("#import_attachments").is(':checked') ? 1 : 0)
                                                },
                                                success: function(data, textStatus, XMLHttpRequest){
                                                    p+= 10;
                                                    $j('.progress-value').html((p) + '%');
                                                    progressbar.val(p);
                                                    $j('.progress-bar-message').html('<div class="alert alert-success"><strong>Import is completed</strong></div>');
                                                },
                                                error: function(MLHttpRequest, textStatus, errorThrown){
                                                }
                                            });
                                        }
                                    },
                                    error: function(MLHttpRequest, textStatus, errorThrown){
                                    }
                                });
                            }
                        } else if(import_opt == 'widgets') {
                            jQuery.ajax({
                                type: 'POST',
                                url: ajaxurl,
                                data: {
                                    action: 'jvfrm_spot_widgetsImport',
                                    example: import_expl
                                },
                                success: function(data, textStatus, XMLHttpRequest){
                                    $j('.progress-value').html((100) + '%');
                                    progressbar.val(100);
                                },
                                error: function(MLHttpRequest, textStatus, errorThrown){
                                }
                            });
                            $j('.progress-bar-message').html('<div class="alert alert-success"><strong>Import is completed</strong></div>');
                        } else if(import_opt == 'options'){
                            jQuery.ajax({
                                type: 'POST',
                                url: ajaxurl,
                                data: {
                                    action: 'jvfrm_spot_optionsImport',
                                    example: import_expl
                                },
                                success: function(data, textStatus, XMLHttpRequest){
                                    $j('.progress-value').html((100) + '%');
                                    progressbar.val(100);
                                },
                                error: function(MLHttpRequest, textStatus, errorThrown){
                                }
                            });
                            $j('.progress-bar-message').html('<div class="alert alert-success"><strong>Import is completed</strong></div>');
                        }else if(import_opt == 'complete_content'){
                            for(var i=1;i<10;i++){
                                var str;
                                if (i < 10) str = 'jvfrm_spot_demo_content_0'+i+'.xml';
                                else str = 'jvfrm_spot_demo_content_'+i+'.xml';
                                jQuery.ajax({
                                    type: 'POST',
                                    url: ajaxurl,
                                    data: {
                                        action: 'jvfrm_spot_dataImport',
                                        xml: str,
                                        example: import_expl,
                                        import_attachments: ($j("#import_attachments").is(':checked') ? 1 : 0)
                                    },
                                    success: function(data, textStatus, XMLHttpRequest){
                                        p+= 10;
                                        $j('.progress-value').html((p) + '%');
                                        progressbar.val(p);
                                        if (p == 90) {
                                            str = 'jvfrm_spot_demo_content_10.xml';
                                            jQuery.ajax({
                                                type: 'POST',
                                                url: ajaxurl,
                                                data: {
                                                    action: 'jvfrm_spot_dataImport',
                                                    xml: str,
                                                    example: import_expl,
                                                    import_attachments: ($j("#import_attachments").is(':checked') ? 1 : 0)
                                                },
                                                success: function(data, textStatus, XMLHttpRequest){
                                                    jQuery.ajax({
                                                        type: 'POST',
                                                        url: ajaxurl,
                                                        data: {
                                                            action: 'jvfrm_spot_otherImport',
                                                            example: import_expl
                                                        },
                                                        success: function(data, textStatus, XMLHttpRequest){
                                                            $j('.progress-value').html((100) + '%');
                                                            progressbar.val(100);
                                                            $j('.progress-bar-message').html('<div class="alert alert-success">Import is completed.</div>');
                                                        },
                                                        error: function(MLHttpRequest, textStatus, errorThrown){
                                                        }
                                                    });
                                                },
                                                error: function(MLHttpRequest, textStatus, errorThrown){
                                                }
                                            });
                                        }
                                    },
                                    error: function(MLHttpRequest, textStatus, errorThrown){
                                    }
                                });
                            }
                        }
                    }
                    return false;
                });
            });
        </script>
		<?php
		do_action( 'jvfrm_spot_admin_helper_page_footer' );
		do_action( 'jvfrm_spot_admin_helper_import_footer' );
	}

	public static function getInstance() {
		if( is_null( self::$hInstance ) ) {
			self::$hInstance = new self;
		}
		return self::$hInstance;
	}

}

if(!function_exists('jvfrm_spot_dataImport'))
{
    function jvfrm_spot_dataImport()
    {
        global $jvfrm_spot_Import;

        if ($_POST['import_attachments'] == 1)
            $jvfrm_spot_Import->attachments = true;
        else
            $jvfrm_spot_Import->attachments = false;

        $folder = "javo-demo/";
        if (!empty($_POST['example']))
            $folder = $_POST['example']."/";

        $jvfrm_spot_Import->import_content($folder.$_POST['xml']);

        die();
    }

    add_action('wp_ajax_jvfrm_spot_dataImport', 'jvfrm_spot_dataImport');
}

if(!function_exists('jvfrm_spot_widgetsImport'))
{
    function jvfrm_spot_widgetsImport()
    {
        global $jvfrm_spot_Import;

        $folder = "javo-demo/";
        if (!empty($_POST['example']))
            $folder = $_POST['example']."/";

        $jvfrm_spot_Import->import_widgets($folder.'widgets.txt',$folder.'custom_sidebars.txt');

        die();
    }

    add_action('wp_ajax_jvfrm_spot_widgetsImport', 'jvfrm_spot_widgetsImport');
}

if(!function_exists('jvfrm_spot_optionsImport'))
{
    function jvfrm_spot_optionsImport()
    {
        global $jvfrm_spot_Import;

        $folder = "javo-demo/";
        if (!empty($_POST['example']))
            $folder = $_POST['example']."/";

        $jvfrm_spot_Import->import_options($folder.'jvfrm_spot_themes_settings.txt');

        die();
    }

    add_action('wp_ajax_jvfrm_spot_optionsImport', 'jvfrm_spot_optionsImport');
}

if(!function_exists('jvfrm_spot_otherImport'))
{
    function jvfrm_spot_otherImport()
    {
        global $jvfrm_spot_Import;

        $folder = "javo-demo/";
        if (!empty($_POST['example']))
            $folder = $_POST['example']."/";

        $jvfrm_spot_Import->import_options($folder.'jvfrm_spot_themes_settings.txt');
        $jvfrm_spot_Import->import_widgets($folder.'widgets.txt',$folder.'custom_sidebars.txt');
        $jvfrm_spot_Import->import_menus($folder.'menus.txt');
        $jvfrm_spot_Import->import_settings_pages($folder.'settingpages.txt');

        die();
    }

    add_action('wp_ajax_jvfrm_spot_otherImport', 'jvfrm_spot_otherImport');
}